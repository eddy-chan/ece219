-----------------------
 Project 2: Clustering
-----------------------

-------------
 Group Info
-------------
Name: Eddy Chan
SID: 604888520
E-mail: eddy.chan@ucla.edu

Name: Kun Zhang
SID: 604890778
E-mail: kxz1399@ucla.edu

-----------------
 How to run code
-----------------
Requirement: 
 * Python 3.6.0+
 * Jupyter 4.4.0+
 * scikit-learn 0.19.1+
 * ipywidgets 7.1.1+

Open each Jupyter Notebooks (in numerical order) and run the code.
Some notebooks depend on pickled objects from previous notebooks so 
it is important to run them in order.
