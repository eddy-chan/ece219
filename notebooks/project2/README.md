## Initial setup
    git config --global user.name "My Name"
    git config --global user.email "user@ucla.edu"
    git config --global core.autocrlf true
## Clone repository
    git clone http://bitbucket.org/eddy-chan/ece219
    cd ece219
## See what changed
    git status
## Stage all changes
    git add -A
## Committing changes
    git commit -s -m "Fixed some bugs."
## Pull changes
    git pull --rebase
## Push committed changes
    git push
## See commit history
    git log